require'nvim-tree'.setup {
  disable_netrw       = true,
  hijack_netrw        = true,
  open_on_setup       = false,
  ignore_ft_on_setup  = {},
  open_on_tab         = false,
  hijack_cursor       = false,
  update_cwd          = false,
  diagnostics = {
    enable = true,
    icons = {
      hint = "",
      info = "",
      warning = "",
      error = "",
    }
  },
  update_focused_file = {
    enable      = true,
    update_cwd  = false,
    ignore_list = {}
  },
  system_open = {
    cmd  = nil,
    args = {}
  },
  filters = {
    dotfiles = false,
    custom = {}
  },
  view = {
    width = 40,
    height = 30,
    hide_root_folder = false,
    side = 'left',
    mappings = {
      custom_only = false,
      list = {}
    }
  },
  actions = {
    open_file = {
      quit_on_open = false,
      window_picker = {
        enable = false
      }
    },
  },
  renderer = {
    indent_markers = {
      enable = false
    },
    highlight_git = true,
    highlight_opened_files = "name",
    root_folder_modifier = ":~",
    add_trailing = false,
    group_empty = false,
    icons = {
      show = {
        file = true,
        folder = true,
        git = true,
        folder_arrow = true,
      }
    }
  },
  respect_buf_cwd = true
}

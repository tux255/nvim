require("nvim-lsp-installer").setup({
  automatic_installation = true, -- automatically detect which servers to install (based on which servers are set up via lspconfig)
  ui = {
    icons = {
      server_installed = "✓",
      server_pending = "➜",
      server_uninstalled = "✗"
    }
  }
})

local lspconfig = require("lspconfig")

local function on_attach(client)
  -- set up buffer keymaps, etc.
  vim.notify(client, "info")
end

local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())


lspconfig.sumneko_lua.setup {
  on_attach = on_attach,
  capabilities = capabilities
}
lspconfig.tsserver.setup {
  on_attach = on_attach,
  capabilities = capabilities
}
lspconfig.eslint.setup {
  on_attach = on_attach,
  capabilities = capabilities
}
lspconfig.tailwindcss.setup {
  on_attach = on_attach,
  capabilities = capabilities
}
lspconfig.solargraph.setup {
  on_attach = on_attach,
  capabilities = capabilities
}

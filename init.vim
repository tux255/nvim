set hidden  " allow buffer switching without saving
set showtabline=2  " always show tabline
set nofoldenable
set foldcolumn=2
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set number
set mouse=a
set clipboard=unnamedplus
set laststatus=2
set noshowmode
set ts=2 sw=2 sts=2 et
set cursorline
" set cursorcolumn
set inccommand=nosplit
" set guicursor=v-c-sm:block,n-i-ci-ve:ver40,r-cr-o:hor20
highlight CursorLine guifg=none guibg=black
highlight Cursor guifg=none guibg=gray

set cscopetag
retab!

" Enable persistent undo so that undo history persists across vim sessions
set undofile
set undodir=~/.config/nvim/undo

"""""""""" VIM SETTINGS
" set tabstop=2 shiftwidth=2 expandtab

noremap <Up> <Nop>
noremap <Down> <Nop>
" noremap <Left> <Nop>
" noremap <Right> <Nop>

" Optimize JS/TS scan
autocmd BufEnter *.{js,jsx,ts,tsx} :syntax sync fromstart
autocmd BufLeave *.{js,jsx,ts,tsx} :syntax sync clear

set completeopt=menu,menuone,noselect

augroup filetypedetect
  au! BufReadPre,BufReadPost,BufRead,BufNewFile *.erb setfiletype eruby.html
  au! BufReadPre,BufReadPost,BufRead,BufNewFile *.feature setfiletype cucumber
  au! BufNewFile,BufRead *.jsx set filetype=javascript.jsx
augroup END
filetype plugin on

let g:python_host_prog  = '/usr/bin/python'
let g:python2_host_prog = '/usr/bin/python2'
let g:python3_host_prog = '/usr/bin/python3'

call plug#begin('~/.config/nvim/plugged')
" Plug 'ray-x/guihua.lua', {'do': 'cd lua/fzy && make' }
" Plug 'ray-x/navigator.lua'
Plug 'liuchengxu/vista.vim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}  " We recommend updating the parsers on update
Plug 'nvim-treesitter/nvim-treesitter-refactor' " this provides "go to def" etc
Plug 'romgrk/nvim-treesitter-context'
" A tree like view for symbols in Neovim using the Language Server Protocol. Supports all your favourite languages.
Plug 'simrat39/symbols-outline.nvim'
" A high-performance color highlighter for Neovim which has no external dependencies! Written in performant Luajit.
Plug 'norcalli/nvim-colorizer.lua'
Plug 'mfussenegger/nvim-dap'
Plug 'rcarriga/nvim-dap-ui'
Plug 'theHamsta/nvim-dap-virtual-text'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'SmiteshP/nvim-gps'
Plug 'folke/lsp-colors.nvim'
" Markups
Plug 'tpope/vim-haml'
Plug 'slim-template/vim-slim'
" Tabs, as understood by any other editor.
Plug 'romgrk/barbar.nvim'
" Plug 'TaDaa/vimade'
Plug 'sjl/gundo.vim'
Plug 'tpope/vim-cucumber'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-rails'
Plug 'airblade/vim-rooter'
Plug 'airblade/vim-gitgutter'
Plug 'kyazdani42/nvim-web-devicons'


Plug 'misterbuckley/vim-definitive'
Plug 'prettier/vim-prettier', {
      \ 'do': 'yarn install',
      \ }
" Plug 'mg979/vim-visual-multi', {'branch': 'master'}
" A collection of common configurations for Neovim's built-in language server client.
Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'RishabhRD/popfix'

Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'winston0410/cmd-parser.nvim'
Plug 'winston0410/range-highlight.nvim'

" For vsnip users.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

Plug 'glepnir/dashboard-nvim'
Plug 'sindrets/diffview.nvim'

Plug 'nvim-lua/plenary.nvim'

Plug 'kyazdani42/nvim-tree.lua'
" This is a simple plugin that helps to end certain structures automatically.
Plug 'tpope/vim-endwise'
Plug 'nvim-lualine/lualine.nvim'
" Insert or delete brackets, parens, quotes in pair.
Plug 'jiangmiao/auto-pairs'
" This plugin adds indentation guides to all lines (including empty lines).
Plug 'lukas-reineke/indent-blankline.nvim'
" Plugin for vim to enable opening a file in a given line
Plug 'bogado/file-line'
" nvim-scrollview is a Neovim plugin that displays interactive vertical scrollbars. The plugin is customizable (see :help scrollview-configuration).
Plug 'dstein64/nvim-scrollview', { 'branch': 'main' }
Plug 'tpope/vim-fugitive'

Plug 'rcarriga/nvim-notify'

" THEMES
Plug 'rockerBOO/boo-colorscheme-nvim'
Plug 'dracula/vim', { 'name': 'dracula' }
Plug 'marko-cerovac/material.nvim'
Plug 'projekt0n/github-nvim-theme'
Plug 'ful1e5/onedark.nvim'
Plug 'folke/tokyonight.nvim', { 'branch': 'main' }
Plug 'rafamadriz/neon'
Plug 'nlknguyen/papercolor-theme'
Plug 'mattly/iterm-colors-pencil'
" Plug 'ludovicchabant/vim-gutentags'
" Plug 'skywind3000/gutentags_plus'
call plug#end()

lua vim.notify = require("notify")
" lua require('navigator').setup{lsp={servers={'tsserver', 'solargraph','tailwindcss','eslint'}}}

" let g:gutentags_modules = ['ctags']
" let g:gutentags_plus_switch = 1

" " GUTENTAGS
" let g:gutentags_cache_dir = expand("~/.cache/nvim/tagfiles/")
" let g:gutentags_add_default_project_roots = 0
" let g:gutentags_project_root=['.git','Gemfile','package.json']
" let g:gutentags_generate_on_new = 1
" let g:gutentags_generate_on_missing = 1
" let g:gutentags_generate_on_write = 1
" let g:gutentags_generate_on_empty_buffer = 0
" let g:gutentags_ctags_extra_args = [
  "       \ '--tag-relative=yes',
  "       \ '--fields=+ailmnS',
  "       \ ]
  " let g:gutentags_ctags_exclude = [
    "       \ '*.git', '*.svg', '*.hg',
    "       \ '*/tests/*',
    "       \ 'build',
    "       \ 'dist',
    "       \ '*sites/*/files/*',
    "       \ 'bin',
    "       \ 'node_modules',
    "       \ 'bower_components',
    "       \ 'cache',
    "       \ 'compiled',
    "       \ 'docs',
    "       \ 'example',
    "       \ 'bundle',
    "       \ 'vendor',
    "       \ '*.md',
    "       \ '*-lock.json',
    "       \ '*.lock',
    "       \ '*bundle*.js',
    "       \ '*build*.js',
    "       \ '.*rc*',
    "       \ '*.json',
    "       \ '*.min.*',
    "       \ '*.map',
    "       \ '*.bak',
    "       \ '*.zip',
    "       \ '*.pyc',
    "       \ '*.class',
    "       \ '*.sln',
    "       \ '*.Master',
    "       \ '*.csproj',
    "       \ '*.tmp',
    "       \ '*.csproj.user',
    "       \ '*.cache',
    "       \ '*.pdb',
    "       \ 'tags*',
    "       \ 'cscope.*',
    "       \ '*.css',
    "       \ '*.less',
    "       \ '*.scss',
    "       \ '*.exe', '*.dll',
    "       \ '*.mp3', '*.ogg', '*.flac',
    "       \ '*.swp', '*.swo',
    "       \ '*.bmp', '*.gif', '*.ico', '*.jpg', '*.png',
    "       \ '*.rar', '*.zip', '*.tar', '*.tar.gz', '*.tar.xz', '*.tar.bz2',
    "       \ '*.pdf', '*.doc', '*.docx', '*.ppt', '*.pptx',
    "       \ ]


    " PRETTIER
    let g:prettier#autoformat = 1
    let g:prettier#autoformat_require_pragma = 0
    let g:prettier#autoformat_config_present = 1
    let g:prettier#exec_cmd_async = 1


    " GITGUTTER
    let g:gitgutter_sign_added = '✚'
    let g:gitgutter_sign_modified = '❆'
    let g:gitgutter_sign_removed = '✖'
    let g:gitgutter_grep = 'rg'

    source ~/.config/nvim/plugins/barbar.vim

    " LSP SAGA SETTINGS
    nnoremap <silent><leader>v :Vista!!<CR>
    nnoremap <silent><leader>O :Files<CR>
    nnoremap <silent><leader>o :GFiles<CR>
    nnoremap <silent><leader>f :Rg<CR>

    let g:fzf_layout = { 'window': { 'width': 0.98, 'height': 0.98 } }
    let g:fzf_history_dir = '~/.local/share/fzf-history'
    command! -bang -nargs=* Rg call fzf#vim#grep("rg --column --line-number --no-heading --color=always --smart-case ".shellescape(<q-args>), 1, {'options': '--delimiter : --nth 4..'}, <bang>0)


    " NVIM LSP SETTINGS
    let g:LanguageClient_serverCommands = {
          \ 'ruby': ['~/.asdf/shims/solargraph', 'stdio'],
          \ }

    " ROOTER SETTINGS "
    let g:rooter_patterns = ['.git/', 'package.json', 'Gemfile']

    set background=dark
    colorscheme material
    " Background transparent
    " hi Normal guibg=NONE ctermbg=NONE

    """""""""" DASHBOARD ETTINGS
    let g:dashboard_default_executive = 'fzf'
    let g:dashboard_session_directory = '/home/ahara/.config/nvim/sessions'
    nmap <Leader>ss :<C-u>SessionSave<CR>
    nmap <Leader>sl :<C-u>SessionLoad<CR>

    " NVIMTree settings
    luafile ~/.config/nvim/lua/_nvimtree.lua
    source ~/.config/nvim/plugins/nvim-tree.vim

    " STALINE/STABLINE SETTINGS
    luafile ~/.config/nvim/lua/_lualine.lua

    "luafile ~/.config/nvim/lua/_symbols_outline.lua NVIM LSP
    luafile ~/.config/nvim/lua/_nvim-lsp.lua

    luafile ~/.config/nvim/lua/lsp-settings.lua

    luafile ~/.config/nvim/lua/_indentline.lua

    luafile ~/.config/nvim/lua/_tree.lua

    " luafile ~/.config/nvim/lua/_gitsigns.lua

    luafile ~/.config/nvim/lua/_treesitter.lua

    luafile ~/.config/nvim/lua/_nvim_cmp.lua

    luafile ~/.config/nvim/lua/_nvim-gps.lua

    luafile ~/.config/nvim/lua/_colorizer.lua

    luafile ~/.config/nvim/lua/_range-highlite.lua

    luafile ~/.config/nvim/lua/_symbols_outline.lua

    " LSP config (the mappings used in the default file don't quite work right)
    nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
    nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
    nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
    nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementation()<CR>
    nnoremap <silent> K <cmd>lua vim.lsp.buf.hover()<CR>
    nnoremap <silent> <C-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
    nnoremap <silent> <C-n> <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
    nnoremap <silent> <C-p> <cmd>lua vim.lsp.diagnostic.goto_next()<CR>
